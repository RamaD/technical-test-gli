import Vue from "vue";
import Axios from "axios";
import store from "./store";
import DashboardPlugin from "./plugins/dashboard-plugin";
import { BootstrapVue, IconsPlugin } from "bootstrap-vue";
import App from "./App.vue";

import Vuex from "vuex";

Vue.use(Vuex);

Vue.prototype.$http = Axios;

// router setup
import router from "./routes/router";
// plugin setup
Vue.use(DashboardPlugin);

Vue.use(BootstrapVue);
Vue.use(IconsPlugin);

import "bootstrap-vue/dist/bootstrap-vue.css";

/* eslint-disable no-new */
new Vue({
  el: "#app",
  render: h => h(App),
  router,
  store
});
