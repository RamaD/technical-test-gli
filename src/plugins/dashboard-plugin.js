import { configure } from "vee-validate";

import GlobalComponents from "./globalComponents";

import GlobalDirectives from "./globalDirectives";

import SideBar from "@/components/SidebarPlugin";

import { BootstrapVue, IconsPlugin } from "bootstrap-vue";

// ALl Custom Styling
import "@/assets/scss/lib.scss";

// Icon
import "@/assets/vendor/nucleo/css/nucleo.css";

// Rules
import { extend } from "vee-validate";
import * as rules from "vee-validate/dist/rules";
import { messages } from "vee-validate/dist/locale/en.json";

Object.keys(rules).forEach(rule => {
  extend(rule, {
    ...rules[rule],
    message: messages[rule]
  });
});

export default {
  install(Vue) {
    Vue.use(GlobalComponents);
    Vue.use(GlobalDirectives);
    Vue.use(SideBar);
    Vue.use(BootstrapVue);
    Vue.use(IconsPlugin);
    configure({
      classes: {
        valid: "is-valid",
        invalid: "is-invalid",
        dirty: ["is-dirty", "is-dirty"]
      }
    });
  }
};
