import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";
import router from "../routes/router";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    snackbars: {
      model: false,
      text: ""
    },
    form: {
      error: {}
    },
    data: [],
    api_path: "http://localhost:3000/",
    status: "",
    token: "",
    user: {},
    table: {
      isLoading: false,
      showData: 10,
      currentPage: 1
    }
  },
  getters: {
    isLoggedIn: state => !!state.token,
    authStatus: state => state.status
  },
  mutations: {
    toggleSnackbar(state, params) {
      state.snackbars.model = params.model;
      state.snackbars.text = params.text;

      setTimeout(function() {
        state.snackbars.model = false;
      }, 2000);
    },
    setTableLoading(state, params) {
      state.table.isLoading = params;
    },
    setTableShowData(state, params) {
      state.table.showData = params;
    },
    setTableCurrentPage(state, params) {
      state.table.currentPage = params;
    },
    setData(state, params) {
      state.data = params;
    },
    setHalaman(state, params) {
      state.halaman = params;
    },
    setItemPesanan(state, params) {
      state.itemPesanan = params;
    },
    formError(state, params) {
      state.form.error = params;
    },
    authRequest(state) {
      state.status = "loading";
    },
    authSuccess(state, params) {
      state.status = "success";
      state.token = params.access_token;
      state.user = params.user;
    },
    authError(state) {
      state.status = "danger";
    },
    logout(state) {
      state.status = "";
      state.token = "";
    }
  },
  actions: {
    async fetchData({ commit, state, dispatch }, params) {
      commit("setTableLoading", true);
      commit("setData", []);
      dispatch("fetch", params)
        .then(val => {
          commit("setData", val.data);
        })
        .finally(() => {
          commit("setTableLoading", false);
        });
    },
    findData({ commit, state, dispatch }, params) {
      let data = state.data;

      let result = state.data.filter(
        item => item.name.toLowerCase().indexOf(params) > -1
      );

      commit("setData", result || []);
    },
    fetch({ commit, state }, params) {
      return new Promise((resolve, reject) => {
        axios({
          url: state.api_path + params.url,
          method: "GET",
          params: params.params
        })
          .then(resp => {
            resolve(resp);
          })
          .catch(err => {
            if (err.response.status == 401) router.push("/login");
            else {
              const vm = new Vue({});

              vm.$bvToast.toast(`Error!`, {
                title: "An Error Occured",
                autoHideDelay: 2000,
                appendToast: false,
                solid: true,
                toaster: "b-toaster-bottom-left",
                variant: "danger"
              });
            }
            reject(err);
          });
      });
    },
    get({ commit, state }, params) {
      return new Promise((resolve, reject) => {
        axios({
          url: state.api_path + params.url,
          method: "GET",
          params: params.params
        })
          .then(resp => {
            resolve(resp);
          })
          .catch(err => {
            if (err.response.status == 401) router.push("/login");
            else if (err.response.status == 422)
              commit("form_error", err.response.data.errors);
            else {
              const vm = new Vue({});

              vm.$bvToast.toast(`Error!`, {
                title: "An Error Occured",
                autoHideDelay: 2000,
                appendToast: false,
                solid: true,
                toaster: "b-toaster-bottom-left",
                variant: "danger"
              });
            }
            reject(err);
          });
      });
    },
    create({ commit, state }, params) {
      return new Promise((resolve, reject) => {
        axios
          .post(state.api_path + params.url, params.params.form, params.headers)
          .then(resp => {
            const vm = new Vue({});

            vm.$bvToast.toast(`Successfully add data`, {
              title: "Success",
              autoHideDelay: 2000,
              appendToast: false,
              solid: true,
              toaster: "b-toaster-bottom-left",
              variant: "success"
            });

            resolve(resp);
          })
          .catch(err => {
            if (err.response.status == 401) router.push("/login");
            else if (err.response.status == 422)
              commit("form_error", err.response.data.errors);
            else {
              const vm = new Vue({});

              vm.$bvToast.toast(`Error!`, {
                title: "An Error Occured",
                autoHideDelay: 2000,
                appendToast: false,
                solid: true,
                toaster: "b-toaster-bottom-left",
                variant: "danger"
              });
            }
            reject(err);
          });
      });
    },
    update({ commit, state }, params) {
      return new Promise((resolve, reject) => {
        axios
          .put(state.api_path + params.url, params.params.form, params.headers)
          .then(resp => {
            const vm = new Vue({});

            vm.$bvToast.toast(`Successfully update data`, {
              title: "Success",
              autoHideDelay: 2000,
              appendToast: false,
              solid: true,
              toaster: "b-toaster-bottom-left",
              variant: "success"
            });

            resolve(resp);
          })
          .catch(err => {
            if (err.response.status == 401) router.push("/login");
            else if (err.response.status == 422)
              commit("form_error", err.response.data.errors);
            else {
              const vm = new Vue({});

              vm.$bvToast.toast(`Error!`, {
                title: "An Error Occured",
                autoHideDelay: 2000,
                appendToast: false,
                solid: true,
                toaster: "b-toaster-bottom-left",
                variant: "danger"
              });
            }
            reject(err);
          });
      });
    },
    delete({ commit, state }, params) {
      return new Promise((resolve, reject) => {
        axios
          .delete(state.api_path + params.url)
          .then(resp => {
            const vm = new Vue({});

            vm.$bvToast.toast(`Successfully delete data`, {
              title: "Success",
              autoHideDelay: 2000,
              appendToast: false,
              solid: true,
              toaster: "b-toaster-bottom-left",
              variant: "success"
            });
            resolve(resp);
          })
          .catch(err => {
            if (err.response.status == 401) router.push("/login");
            else if (err.response.status == 422)
              commit("form_error", err.response.data.errors);
            else {
              const vm = new Vue({});

              vm.$bvToast.toast(`An Error Occured`, {
                title: "Error!",
                autoHideDelay: 2000,
                appendToast: false,
                solid: true,
                toaster: "b-toaster-bottom-left",
                variant: "danger"
              });
            }
            reject(err);
          });
      });
    },
    login({ commit, state }, user) {
      return new Promise((resolve, reject) => {
        commit("auth_request");
        axios({
          url: state.api_path + "login",
          data: user,
          method: "POST"
        })
          .then(resp => {
            const token = resp.data.access_token;
            localStorage.setItem("token", token);
            axios.defaults.headers.common["Authorization"] = "Bearer " + token;
            commit("auth_success", resp.data);
            resolve(resp);
          })
          .catch(err => {
            if (err.response.status == 422)
              commit("form_error", err.response.data.errors);
            else {
              const vm = new Vue({});

              vm.$bvToast.toast(`An Error Occured`, {
                title: "Error!",
                autoHideDelay: 2000,
                appendToast: false,
                solid: true,
                toaster: "b-toaster-bottom-left",
                variant: "danger"
              });
            }
            commit("auth_error");
            localStorage.removeItem("token");
            reject(err);
          });
      });
    },
    logout({ commit }) {
      return new Promise(resolve => {
        commit("logout");
        localStorage.removeItem("token");
        delete axios.defaults.headers.common["Authorization"];
        resolve();
      });
    }
  }
});
