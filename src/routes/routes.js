import DashboardLayout from "@/views/Layout/DashboardLayout.vue";
import NoLayout from "@/views/Layout/NoLayout.vue";

const routes = [
  {
    path: "/",
    redirect: "role",
    component: DashboardLayout,
    children: [
      {
        path: "/user",
        name: "User",
        component: () => import("../views/Pages/User.vue")
      },
      {
        path: "/role",
        name: "Role",
        component: () => import("../views/Pages/Role.vue")
      }
    ]
  }
];

export default routes;
